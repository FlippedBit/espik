import { Avatar } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import db from './firebase';
import './Room.css'
import { useStateValue } from './StateProvider';
import firebase from "firebase";

function Room() {
    //state of the input allows for capturing message
    const [input, setInput] = useState("");
    //set the seed constant
    const [seed, setSeed] = useState('');
    //get roomId hook for routing
    const { roomId } = useParams();
    //get the room data into the room component
    const [roomName, setRoomName] = useState("");
    //gets and manages all messages
    const [messages, setMessages] = useState([]);
    // get the user from data layer
    const [{user}, dispatch] = useStateValue();

    //set the room name
    useEffect(() => {
        if (roomId) {
            db.collection("rooms")
                .doc(roomId)
                .onSnapshot((snapshot) => setRoomName(snapshot.data().name));

            db.collection('rooms')
                .doc(roomId)
                .collection('messages')
                .orderBy('timestamp', 'asc')
                .onSnapshot((snapshot) =>
                    setMessages(snapshot.docs.map((doc) => doc.data()))
                );
        }
    }, [roomId]);

    //message sending function
    const sendMessage = (e) => {
        e.preventDefault();
        //submit the message
        db.collection('rooms').doc(roomId).collection('messages').add({
            message: input,
            username: user.displayName,
            timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        })

        //clear the input after sending
        setInput("");
    }

    //calculate a new seed every time the component is loaded
    useEffect(() => {
        setSeed(Math.floor(Math.random() * 5000))
    }, [roomId])


    return (
        <div className="room">

            {/* room header */}
            <div className="room__header">
                <Avatar src={`https://avatars.dicebear.com/api/jdenticon/${seed}.svg`} />
                <div className="room__headerInfo">
                    <h3>{roomName}</h3>
                    <p>
                        Último mensaje {" "}
                        {new Date(messages[messages.length -1]?.timestamp?.toDate()
                        ).toUTCString()}
                    </p>
                </div>
            </div>

            {/* room content */}
            <div className="room__content">
                {messages.map(message => (
                    <p className={`room__message ${message.username == user.displayName && 'room__messageSender'}`}>
                        <span className="room__messageName">
                            {message.username}
                        </span>
                        {message.message}
                        <span className="room__messageTime">
                            {new Date(message.timestamp?.toDate()).toUTCString()}
                        </span>
                    </p>
                ))}
            </div>

            {/* room footer */}
            <div className="room__footer">
                <form>
                    {/* the onChange event sets the input constant value to the input value */}
                    <input value={input} onChange={e => setInput(e.target.value)} type="text" placeholder="Escriba su mensaje" />
                    <button onClick={sendMessage} type="submit">Enviar</button>
                </form>
            </div>
        </div>
    )
}

export default Room
