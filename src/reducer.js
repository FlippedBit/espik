// this is the state of the data layer at boot
export const initialState = {
    user: null,
};

// stablish the type of actions that allow to push data into the data layer
export const actionTypes = {
    SET_USER: "SET_USER"
};

// if the executed action is a SET_USER action, this sets the user data while keeping the current state
const reducer = (state, action) => {
    switch(action.type) {
        case actionTypes.SET_USER:
            return {
                ...state,
                user: action.user,
            };
        default:
            return state;
    }
};


export default reducer;