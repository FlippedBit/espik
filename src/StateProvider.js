import React, { createContext, useContext, useReducer } from "react";

// initializes the data layer
export const StateContext = createContext();

// sets up so data can be put into the children, in this case my app
export const StateProvider = ({ reducer, initialState, children }) => (
    <StateContext.Provider value = {useReducer(reducer, initialState)}>
        {children}
    </StateContext.Provider>
);

// allows for data to be extracted from the data layer
export const useStateValue = () => useContext(StateContext);