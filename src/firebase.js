import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyBpBVPOR2LHOig2Sjp8NLp-zSZ9EkcSSR4",
    authDomain: "espik-885cf.firebaseapp.com",
    databaseURL: "https://espik-885cf.firebaseio.com",
    projectId: "espik-885cf",
    storageBucket: "espik-885cf.appspot.com",
    messagingSenderId: "577531762770",
    appId: "1:577531762770:web:431415c80504724dc3278b",
    measurementId: "G-RBZ5ZHKB53"
  };
// initialize the firebase backend
const firebaseApp = firebase.initializeApp(firebaseConfig);
// setup the dabase object
const db = firebaseApp.firestore();
// setup authentication handler
const auth = firebase.auth();
// set authentication provider as Google Login
const provider = new firebase.auth.GoogleAuthProvider();

export { auth, provider };
export default db;


