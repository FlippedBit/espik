import { createMuiTheme, ThemeProvider } from '@material-ui/core';
import React, { useState } from 'react';
import './App.css';
import Sidebar from './Sidebar';
import Room from './Room';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Login from './Login';
import { useStateValue } from './StateProvider';

function App() {
  //set up the dark theme for material ui
  const darkTheme = createMuiTheme({
    palette: {
      type: 'dark',
    },
  });

  const [{user}, dispatch] = useStateValue();

  return (
    //I'll use BEM naming for this project
    <ThemeProvider theme={darkTheme}>
      <div className="app">
        {/* checks for a loged in user and renders accordingly */}
        {!user ? (
          <Login/>
        ) : (
            <div className="app__body">
              {/* routing for the rooms */}
              <Router>
                {/* sidebar always renders */}
                <Sidebar />
                <Switch>
                  <Route path="/rooms/:roomId">
                    {/* room window */}
                    <Room />
                  </Route>

                  <Route path="/">
                    {/* room window */}
                    <Room />
                  </Route>

                </Switch>
              </Router>

            </div>
          )}
      </div>
    </ThemeProvider>
  );
}

export default App;
