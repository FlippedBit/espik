import React from 'react';
import './Login.css';
import logo from './images/logo.png';
import { Button } from '@material-ui/core';
import { auth, provider } from './firebase';
import { actionTypes } from './reducer';
import { useStateValue } from './StateProvider';

function Login() {

    const [{}, dispatch] = useStateValue();

    //signIn function
    const signIn = () =>{
        auth.signInWithPopup(provider)
        .then((result) => {
            dispatch({
                type: actionTypes.SET_USER,
                user: result.user,
            })
        }).catch(error => alert(error.message));
    }

    return (
        <div className="login">
            <div className="login__container">
                <img src={logo} alt="Logo de la aplicación"/>
                <div className="login__text">
                    <h1>Inicia sesión en Espik</h1>
                </div>
                <Button onClick={signIn}>
                    Iniciar con Google
                </Button>
            </div>
        </div>
    )}

export default Login
