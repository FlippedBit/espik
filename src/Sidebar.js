import { Avatar, IconButton } from '@material-ui/core'
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ChatIcon from '@material-ui/icons/Chat';
import React, { useEffect, useState } from 'react'
import './Sidebar.css'
import { SearchOutlined } from '@material-ui/icons';
import SidebarRoom from './SidebarRoom';
import db from './firebase';
import { useStateValue } from './StateProvider';

function Sidebar() {
    //manage all rooms in database through state
    const [rooms, setRooms] = useState([]);
    //get the logged in user from the data layer
    const [{ user }, dispatch] = useStateValue();

    useEffect(() => {
        //get all the rooms and listen for changes to snapshots
        const unsubscribe = db.collection('rooms').onSnapshot(snapshot => (
            //basically go through all the docs and return each one as an object
            setRooms(snapshot.docs.map(doc =>
                ({
                    id: doc.id,
                    data: doc.data()
                })
            ))
        ));

        return () => {
            unsubscribe();
        }
    }, [])

    return (
        <div className="sidebar">
            {/* header with avatar and icons */}
            <div className="sidebar__header">
                <Avatar src={user?.photoURL}/>
                <div className="sidebar__headerIcons">
                    <h2>{user?.displayName}</h2>
                </div>
            </div>

            {/* room list */}
            <div className="sidebar__rooms">
                <SidebarRoom addNewRoom />
                {/* go through the rooms array and map their data into SidebarRoom components */}
                {rooms.map(room => (
                    <SidebarRoom key={room.id} id={room.id} name={room.data.name} />
                ))}
            </div>
        </div>
    )
}

export default Sidebar