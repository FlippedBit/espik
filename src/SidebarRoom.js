import { Avatar } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import db from './firebase';
import './SidebarRoom.css'

function SidebarRoom({ id, name, addNewRoom }) {
    //set the seed constant
    const [seed, setSeed] = useState('');
    //messages state for timestamp retrieval
    const [messages, setMessages] = useState([]);

    //get the timestamp of most recent message
    useEffect(() => {
        if (id) {
            db.collection('rooms')
                .doc(id)
                .collection('messages')
                .orderBy('timestamp', 'desc')
                .onSnapshot((snapshot) => 
                   setMessages(snapshot.docs.map((doc) => doc.data()))
            );
        }
    }, [id])

    //calculate a new seed every time the component is loaded
    useEffect(() => {
        setSeed( Math.floor( Math.random() * 5000 ))
    }, [])

    //create a new chat room
    const createRoom = () => {
        //ask for the room name
        const roomName = prompt('Ingrese el nombre de la sala')
        //if the name is prompted, go to database
        if (roomName) {
            db.collection('rooms').add({
                name: roomName,
            })
        }
    }

    return !addNewRoom ? (
        <Link to={`/rooms/${id}`}>
            <div className="sidebarRoom">
                <Avatar src={`https://avatars.dicebear.com/api/jdenticon/${seed}.svg`}/>
                <div className="sidebarRoom__info">
                    <h2>{name}</h2>
                    <p>{messages[0]?.message} </p>
                </div>
            </div>
        </Link>
    ): (
        <div onClick={createRoom} className="sidebarRoom">
            <h2>Añadir sala</h2>
        </div>
    )
}

export default SidebarRoom
